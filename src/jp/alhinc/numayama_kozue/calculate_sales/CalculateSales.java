package jp.alhinc.numayama_kozue.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CalculateSales {
	public static void main(String[] args) {
		// 売上集計課題フォルダを宣言
//		System.out.println("‪開く " + args[0]);
//
		// 値を格納
        HashMap<String,String> branchlst = new HashMap<>();
        HashMap<String,Long> branchout = new HashMap<>();
		BufferedReader br = null;

		try {
			// 支店定義ファイル"branch.lst"の読み込み
			File file1 = new File(args[0], "branch.lst");
			if(!file1.exists()) {
				System.out.println("支店定義ファイルが存在しません。");
				return;
			}

			// 支店定義ファイルの読み込み指示
			FileReader fr = new FileReader(file1);
			// ファイル内のテキストを1行ずつ読み込み指示
			br = new BufferedReader(fr);
            String list;
            // 行の読み込みを繰り返す指示：listに支店定義ファイルの情報が入る
            while ((list = br.readLine()) != null) {
//            	System.out.println(list);
            	// ,で文を分割
				String[] list1 = list.split(",");
				if((list1.length != 2) || (!list1[0].matches("[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return;
				}

		        // 要素とキーをマップに登録
		        branchlst.put(list1[0],list1[1]);
		        branchout.put(list1[0],0l);

            }

            } catch(IOException e) {
            	System.out.println("予期せぬエラーが発生しました。");
            	return;
            } finally {
            	if(br != null) {
            		try {
            			br.close();
            		} catch (IOException e) {
            			return;
            		}
            	}
            }

            // 売上集計課題を読み込む
            File file2 = new File(args[0]);
            File fileList[] = file2.listFiles();

            for(int i = 0;i < fileList.length ; i++) {
            	// 0〜9の8桁、末尾.rcdのファイルを検索
            	if(fileList[i].getName().matches("^[0-9]{8}.rcd$")) {
            		// fileList[i]：検索したデータのアドレス
//            		System.out.println(fileList[i]);
            		try {
            			// fr1：売上ファイルから抽出した売上額
            			FileReader fr1 = new FileReader(fileList[i]);
            			br = new BufferedReader(fr1);
            			// listの"lines"を定義
            			List<String> lines = new ArrayList<String>();

            			String line;
            			while((line = br.readLine()) != null) {
            				// linesにデータを追加
            				lines.add(line);
            			}
            			if(lines.size()!=2) {
            				System.out.println("<該当ファイル名>のフォーマットが不正です。");
            				return;
            			}
            			// "n"に今までの売上金を代入。
            			Long n = branchout.get(lines.get(0));
//            			System.out.println(branchout.get(lines.get(0)));
            			// 今までの売上金と当月の売上金を合計する。
            			n += Long.parseLong(lines.get(1));
            			System.out.println(n);
            			// 合計金額が10桁を超えるか判断する。
            			if(n >= 1000000000) {
            				System.out.println("合計金額が10桁を超えました。");
            				return;
            			}
            			// 支店に該当するか判断する。
            			if(!branchlst.containsKey(lines.get(0))) {
            				System.out.println("<該当ファイル名>の支店コードが不正です。");
            				return;
            			}
            			// "branchout"に合計額を入れる。
            			branchout.put(lines.get(0),n);

            		} catch(IOException e) {
            			System.out.println("予期せぬエラーが発生しました。");
            			return;
            		} finally {
            			if(br != null) {
            				try {
            					br.close();
            				} catch(IOException e) {
            					return;
            				}
            			}
            		}
            	}
            }
            BufferedWriter bw = null;
            try {
            	// "bransh.out"を作成、書き込み先を指定
            	File file3 = new File(args[0],"branch.out");
            	FileWriter fw = new FileWriter(file3);
            	bw = new BufferedWriter(fw);

            	// branch.outに書き込む
            	for(String nkey : branchlst.keySet()) {
            		bw.write(nkey+","+branchlst.get(nkey)+","+branchout.get(nkey));
            		System.out.println(nkey+","+branchlst.get(nkey)+","+branchout.get(nkey));
            		bw.write("\n");
            	}
            } catch(IOException e) {
        			System.out.println("予期せぬエラーが発生しました。");
        			return;
        		} finally {
        			if(bw != null) {
        				try {
        					bw.close();
        				} catch(IOException e) {
        					return;
        				}
        			}
        		}
            }
	}

